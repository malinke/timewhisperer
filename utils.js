/*  GNOME Shell Extension for the great Timewarrior application
 *
 * Copyright (C) 2022
 *     Max Linke < >
 *
 * This file is part of taskwhisperer-extension.
 *
 * taskwhisperer-extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * taskwhisperer-extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with taskwhisperer-extension.  If not, see <http://www.gnu.org/licenses/>.
 */
"use strict";
const { GObject, Gio, GLib } = imports.gi;
const Signals = imports.signals;

var EventHandler = class EventHandler {};

Signals.addSignalMethods(EventHandler.prototype);

var setTimeout = (func, time) =>
  GLib.timeout_add(GLib.PRIORITY_DEFAULT, time, () => {
    func.call();

    return GLib.SOURCE_REMOVE;
  });

var clearTimeout = (timerId) => {
  GLib.source_remove(timerId);

  return null;
};
// partially copied from https://wiki.gnome.org/AndyHolmes/Sandbox/SpawningProcesses
var run = async ({ command, input = null, timeout = 10 }) => {
  try {
    const [ok, argv] = GLib.shell_parse_argv(command);

    // We'll assume we want output, or that returning none is not a problem
    let flags =
      Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE;

    // If we aren't given any input, we don't need to open stdin
    if (input !== null) {
      flags |= Gio.SubprocessFlags.STDIN_PIPE;
    }

    const proc = new Gio.Subprocess({
      argv,
      flags,
    });

    const cancellable = new Gio.Cancellable();

    // Classes that implement GInitable must be initialized before use, but
    // an alternative in GJS is to just use Gio.Subprocess.new(argv, flags)
    proc.init(cancellable);

    const cancelTimeOutId = setTimeout(
      () => cancellable.cancel(),
      timeout * 1000
    );

    const result = await new Promise((resolve, reject) => {
      proc.communicate_utf8_async(input, cancellable, (proc, res) => {
        try {
          let [ok, stdout, stderr] = proc.communicate_utf8_finish(res);

          const returnCode = proc.get_exit_status();

          if (returnCode !== 0) {
            resolve({ error: stderr || stdout });
          }

          clearTimeout(cancelTimeOutId);
          resolve({ output: stdout });
        } catch (e) {
          logError(e);
          reject(e);
        }
      });
    });

    return result;
  } catch (e) {
    logError(e);
    return { error: e.toString() };
  }
};
