/*  GNOME Shell Extension for the great Timewarrior application
 *
 * Copyright (C) 2022
 *     Max Linke < >
 *
 * This file is part of taskwhisperer-extension.
 *
 * taskwhisperer-extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * taskwhisperer-extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with taskwhisperer-extension.  If not, see <http://www.gnu.org/licenses/>.
 */

/* exported init */
"use strict";

const GETTEXT_DOMAIN = "my-indicator-extension";

const { Clutter, GObject, St, Gio, GLib } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const _ = ExtensionUtils.gettext;

const timewarrior = Me.imports.timewarrior;
const { EventHandler } = Me.imports.utils;

const IS_TRACKING = true;
const NOT_TRACKING = false;

var menuItem = GObject.registerClass(
  class menuItem extends St.BoxLayout {
    _init(main_event_handler) {
      super._init({
        style_class: "menu-item",
        x_expand: true,
        y_align: Clutter.ActorAlign.CENTER,
        reactive: true,
        y_expand: true,
      });
      this._main_event_handler = main_event_handler;
      this._set_id = this._main_event_handler.connect(
        "refresh-menu-tracking-status",
        (_, is_tracking) => {
          this._is_tracking = is_tracking;
          this._update_info_box();
        }
      );
    }

    async _update_info_box() {
      this.destroy_all_children();
      const taskInformationBox = new St.BoxLayout({
        style_class: "task-information-box",
        vertical: false,
        x_expand: true,
        y_expand: true,
        y_align: Clutter.ActorAlign.CENTER,
      });
      const icon_path =
        this._is_tracking == IS_TRACKING
          ? `${Me.path}/icons/taskwarrior-symbolic-green.svg`
          : `${Me.path}/icons/taskwarrior-symbolic-red.svg`;
      const icon = new St.Icon({
        y_align: Clutter.ActorAlign.CENTER,
        y_expand: true,
        gicon: Gio.icon_new_for_string(icon_path),
        style_class: "menu-icon",
      });
      taskInformationBox.add_child(icon);

      const label = new St.Label({
        style_class: "menu-label",
        y_align: Clutter.ActorAlign.CENTER,
        y_expand: true,
        text: this._duration,
      });
      taskInformationBox.add_child(label);

      this.add_child(taskInformationBox);
    }
    _onDestroy() {
      if (this._set_id) {
        this._main_event_handler.disconnect(this._set_id);
      }
    }
    async load_data() {
      this._is_tracking = await timewarrior.is_tracking();
      this._duration = await timewarrior.duration(":day");
      this._update_info_box();
    }
  }
);

const Indicator = GObject.registerClass(
  class Indicator extends PanelMenu.Button {
    _init() {
      super._init(0.0, _("Timewhisperer Extension"));
      this._main_event_handler = new EventHandler();
      this._icon = new menuItem(this._main_event_handler);

      this.add_child(this._icon);
      this._icon.load_data();

      this._auto_refresh_id = GLib.timeout_add_seconds(
        GLib.PRIORITY_DEFAULT,
        60,
        async () => {
          this._icon.load_data();
          return GLib.SOURCE_CONTINUE;
        }
      );

      let start = new PopupMenu.PopupMenuItem(_("Start Track Time"));
      start.connect("activate", (item, event) => {
        timewarrior.start_tracking();
        this._main_event_handler.emit(
          "refresh-menu-tracking-status",
          IS_TRACKING
        );
        Main.notify("Start time tracking");
      });
      this.menu.addMenuItem(start);

      let stop = new PopupMenu.PopupMenuItem(_("Stop Track Time"));
      stop.connect("activate", (item, event) => {
        timewarrior.stop_tracking();
        this._main_event_handler.emit(
          "refresh-menu-tracking-status",
          NOT_TRACKING
        );
        Main.notify("Stop time tracking");
      });
      this.menu.addMenuItem(stop);
    }

    _onDestroy() {
      if (this._auto_refresh_id) {
        Mainloop.source_remove(this._auto_refresh_id);
      }
      super._onDestroy();
    }
  }
);

class Extension {
  constructor(uuid) {
    this._uuid = uuid;

    ExtensionUtils.initTranslations(GETTEXT_DOMAIN);
  }

  enable() {
    this._indicator = new Indicator();
    Main.panel.addToStatusArea(this._uuid, this._indicator);
  }

  disable() {
    this._indicator.destroy();
    this._indicator = null;
  }
}

function init(meta) {
  return new Extension(meta.uuid);
}
