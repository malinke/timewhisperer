/*  GNOME Shell Extension for the great Timewarrior application
 *
 * Copyright (C) 2022
 *     Max Linke < >
 *
 * This file is part of taskwhisperer-extension.
 *
 * taskwhisperer-extension is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * taskwhisperer-extension is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with taskwhisperer-extension.  If not, see <http://www.gnu.org/licenses/>.
 */
"use strict";

const ExtensionUtils = imports.misc.extensionUtils;
const Me = imports.misc.extensionUtils.getCurrentExtension();

const { GLib } = imports.gi;

const { run } = Me.imports.utils;

var Interval = class Interval {
  get is_open() {
    return this._is_open;
  }
  get duration() {
    return this._end.difference(this._start);
  }
  constructor(interval_data) {
    interval_data = interval_data || {};
    this._id = interval_data["id"];
    this._start = GLib.DateTime.new_from_iso8601(interval_data["start"], null);
    const end = interval_data["end"];
    this._end =
      end != null
        ? GLib.DateTime.new_from_iso8601(end, null)
        : GLib.DateTime.new_now_local();
    this._is_open = end == null;
  }
};

var human_readable_duration = (dur) => {
  const hours = Math.floor(dur / GLib.TIME_SPAN_HOUR);
  const minutes = Math.floor(
    (dur % GLib.TIME_SPAN_HOUR) / GLib.TIME_SPAN_MINUTE
  );
  return hours + "h " + minutes + "m";
};

var json_report = async (range = "") => {
  try {
    let command = ["timew", "export", range.toString()].join(" ");
    const { output, error } = await run({ command });
    return JSON.parse(output);
  } catch (e) {
    logError(e);
    return false;
  }
};

var load_intervals = async (range = "") => {
  try {
    const tracked_times = await json_report(range);
    const intervals = tracked_times.map((t) => new Interval(t));
    return intervals;
  } catch (e) {
    logError(e);
    return null;
  }
};

var duration = async (range = ":day") => {
  try {
    const intervals = await load_intervals(range);
    const total_duration = intervals.reduce(
      (accumulator, current) => accumulator + current.duration,
      0
    );
    return human_readable_duration(total_duration);
  } catch (e) {
    logError(e);
  }
};

var is_tracking = async () => {
  try {
    const tracked_time = await json_report();
    const interval = new Interval(tracked_time[tracked_time.length - 1]);
    return interval.is_open;
  } catch (e) {
    logError(e);
    return false;
  }
};

var start_tracking = async () => {
  let command = ["timew", "start"].join(" ");
  try {
    const { output, error } = await run({ command });
    if (error != null) {
      return false;
    }
    return true;
  } catch (e) {
    logError(e);
    return false;
  }
};

var stop_tracking = async () => {
  let command = ["timew", "stop"].join(" ");
  try {
    const { output, error } = await run({ command });
    if (error != null) {
      return false;
    }
    return true;
  } catch (e) {
    logError(e);
    return false;
  }
};
