.PHONY: format

format: 
	npx prettier --write .
	
test:
	dbus-run-session -- gnome-shell --nested --wayland
